from unittest import TestCase
from selenium import webdriver
from page.page_one import yandexPage
from time import sleep


class TestYandexSearch(TestCase):
    main_page = None
    driver = None
    yandex = "https://yandex.ru/"

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome()
        cls.main_page = yandexPage(cls.driver)

    def setUp(self):
        self.driver.get(url=self.yandex)
        print("Открываем сайт yandex.ru")

    def testOneYandexSearch(self):
        self.main_page.enterText("Тензор")
        # вводим текст и проверяем наличие поисковой строки

        sleep(3)

        self.main_page.checkSuggest()
        # проверяем наличие меню подсказок

        sleep(3)

        self.main_page.pressEnter()
        # нажимаем ENTER

        sleep(3)

        self.main_page.checkLinks()
        # проверяем есть ли ссылка в первых 5 результатах

        sleep(3)

        print("Тест testOneYandexSearch пройден успешно")

    def testTwoYandexImages(self):
        sleep(3)

        self.main_page.checkImageLink()
        # проверяем, есть ли ссылка "Картинки" есть ссылка на странице

        sleep(2)

        self.main_page.clickLink()
        # кликаем по ссылке

        sleep(2)

        self.main_page.switchToWindow()
        # переключаемся на вторую вкладку

        self.main_page.checkUrl("https://yandex.ru/images/")
        # проверяем url адрес

        firstCatalogName = self.main_page.getNameImageCatalog()

        sleep(2)

        self.main_page.clickOnCatalog()
        # переходим по первому каталогу картинок

        sleep(2)

        self.main_page.checkInputValue(firstCatalogName)
        # проверили что значение в поисковой строке соответствует названию каталога

        sleep(2)

        self.main_page.openImage()
        # открываем первую картинку

        sleep(2)

        self.main_page.checkImageIsDisplayed()
        # проверяем, открылась ли картинка

        sleep(2)

        imageSrc = self.main_page.getImageSrc()

        self.main_page.nextImage()
        # переходим к следующей картинке

        sleep(2)

        self.main_page.prevImage()
        # переходим к предыдущей картинке

        sleep(2)

        self.main_page.checkImageSrc(imageSrc)
        # проверяем, таже ли эта картинки, что была первой

        sleep(2)

        print("Тест testTwoYandexImages пройден успешно")

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()