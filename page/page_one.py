from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class yandexPage:
    search = (By.ID, "text")
    select = (By.CLASS_NAME, "mini-suggest__popup-content")
    result_list = (By.CSS_SELECTOR, ".serp-item .organic__path")
    images = (By.LINK_TEXT, "Картинки")
    img_catalog = (By.CLASS_NAME, "PopularRequestList-Shadow")
    img_catalog_value = (By.CSS_SELECTOR, ".PopularRequestList-SearchText")
    search_input = (By.CLASS_NAME, "input__control")
    images_links = (By.CLASS_NAME, "serp-item__link")
    circle_next = (By.CLASS_NAME, "CircleButton_type_next")
    circle_prev = (By.CLASS_NAME, "CircleButton_type_prev")
    image = (By.CLASS_NAME, "MMImageWrapper")
    imageSrc = (By.CLASS_NAME, "MMImage-Origin")

    def __init__(self, driver):
        self.driver = driver

    def enterText(self, text):
        search = self.driver.find_element(self.search[0], self.search[1])
        search.send_keys(text)
        assert search.is_displayed(), "Строка поиска отсутствует"
        print("Вводим текст и проверяем наличие поисковой строки")
    # вводим текст и проверяем наличие поисковой строки

    def checkSuggest(self):
        select = self.driver.find_element(self.select[0], self.select[1])
        assert select.is_displayed(), "Меню подсказок не отображается"
        print("Проверяем наличие меню подсказок")
    # проверяем наличие меню подсказок

    def pressEnter(self):
        search = self.driver.find_element(self.search[0], self.search[1])
        search.send_keys(Keys.ENTER)
        print("Нажимаем ENTER")
    # нажимаем ENTER

    def checkLinks(self):
        result_list = self.driver.find_elements(self.result_list[0], self.result_list[1])
        exist = False
        for i in range(5):
            if "tensor.ru" in result_list[i].text:
                exist = True
                break
        assert exist, "В первых пяти результатах нет ссылки на Тензор"
        print("Проверяем есть ли ссылка в первых 5 результатах")
    # проверяем есть ли ссылка в первых 5 результатах

    def checkImageLink(self):
        images = self.driver.find_element(self.images[0], self.images[1])
        assert images.is_displayed(), "Ссылка 'Картинки' не присутствует на стринице"
        print("Проверяем, есть ли ссылка 'Картинки' на странице")
    # проверяем, есть ли ссылка "Картинки" на странице

    def clickLink(self):
        images = self.driver.find_element(self.images[0], self.images[1])
        images.click()
        print("Кликаем по ссылке")
    # кликаем по ссылке

    def switchToWindow(self):
        tabs = self.driver.window_handles
        self.driver.switch_to.window(tabs[1])
        print("Переключаемся на вторую вкладку")
    # переключаемся на вторую вкладку

    def checkUrl(self, url):
        now_url = self.driver.current_url
        assert url in now_url, "URL не соответствует формату https://yandex.ru/images/"
        print("Проверка url адреса")
    # проверяем url адрес

    def clickOnCatalog(self):
        img_catalog = self.driver.find_element(self.img_catalog[0], self.img_catalog[1])
        img_catalog.click()
        print("Переходим по первому каталогу картинок")
    # переходим по первому каталогу картинок

    def getNameImageCatalog(self):
        img_catalog_value = self.driver.find_element(self.img_catalog_value[0], self.img_catalog_value[1])
        print("Получаем имя перврго каталога")
        return img_catalog_value.text
    # получаем имя перврго каталога

    def checkInputValue(self, firstCatalogName):
        search_input = self.driver.find_element(self.search_input[0], self.search_input[1])
        assert search_input.get_attribute("value") == firstCatalogName, "Значение в поисковой строке не соответствует названию каталога"
        print("Проверяем, что значение в поисковой строке соответствует названию каталога")
    # проверяем, что значение в поисковой строке соответствует названию каталога

    def openImage(self):
        image = self.driver.find_element(self.images_links[0], self.images_links[1])
        image.click()
        print("Открываем первую картинку")
    # открываем первую картинку

    def checkImageIsDisplayed(self):
        image = self.driver.find_element(self.image[0], self.image[1])
        assert image.is_displayed(), "Картинка не открылась"
        print("Проверяем, открылась ли картинка")
    # проверяем, открылась ли картинка

    def getImageSrc(self):
        imageScr = self.driver.find_element(self.imageSrc[0], self.imageSrc[1])
        print("Получаем значение атрибута src у картинки")
        return imageScr.get_attribute("src")
    # получаем значение атрибута src у картинки

    def nextImage(self):
        circle_next = self.driver.find_element(self.circle_next[0], self.circle_next[1])
        circle_next.click()
        print("Переходим к следующей картинке")
    # переходим к следующей картинке

    def prevImage(self):
        circle_prev = self.driver.find_element(self.circle_prev[0], self.circle_prev[1])
        circle_prev.click()
        print("Переходим к предыдущей картинке")
    # переходим к предыдущей картинке

    def checkImageSrc(self, imageSrc):
        imageScr = self.driver.find_element(self.imageSrc[0], self.imageSrc[1])
        assert imageSrc == imageScr.get_attribute("src"), "Это другая картинка"
        print("Проверяем, таже ли эта картинки, что была первой")
    # проверяем, таже ли эта картинки, что была первой